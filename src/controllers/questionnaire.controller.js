const genericCrud = require('./generic.controller');
const { Questionnaire } = require('../models');

module.exports = {
	...genericCrud(Questionnaire)
};
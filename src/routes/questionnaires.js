const router = require('express-promise-router')();
const { questionnaire } = require('../controllers');

//Получение и возращение значений по указанным методам
router.route('/:id').get(questionnaire.get);
router.route('/').get(questionnaire.getAll);
router.route('/:id').put(questionnaire.update);
router.route('/').post(questionnaire.create);
router.route('/:id').delete(questionnaire.delete);

module.exports = router;
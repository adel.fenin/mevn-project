//Описываю схему в БД
const {
	model, 
	Schema, 
	Schema: { 
		Types: { ObjectId },
	},
} = require('mongoose');

const schema = new Schema({
	firstName: {
		type: String,
		required: true,
	},
	lastName: {
		type: String,
		required: true,
	},
	middleName: {
		type: String,
		default: '',
	},
	birthDate: {
		type: String,
		required: true,
	},
	description: {
		type: String,
		default: '',
	}
});

module.exports = model('Questionnaire', schema);
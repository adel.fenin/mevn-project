export default {
	data() {
		return {
			page: +this.$route.query.page || 1,
			pageSize: 5,
			pageCount: 0,
			allItems: [],
			paginatedItems: []
		}
	},
	methods: {
		//Метод для отображения определенных элементов исходя из страницы, на которой мы находимся
		pageChangeHandler(page) {
			this.$router.push(`${this.$route.path}?page=${page}`)
			this.paginatedItems = this.allItems[page -1] || this.allItems[0]
		},
		setupPagination(allItems) {
			this.allItems = _.chunk(allItems, this.pageSize)
			this.pageCount = _.size(this.allItems)
			//Отсортированные анкеты
			this.paginatedItems = this.allItems[this.page - 1] || this.allItems[0]
		}
	},
}



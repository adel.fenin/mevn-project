export const mixinValidation = {
	//Метод с прописанными условиями для реализации валидации
	methods: {
		validationMethod() {
			//checkValidate отвечающий за прохождения валидации
			var checkValidate = false
			if(this.formModel.fullName) {
				var myArray = this.formModel.fullName.split(" ");
				if(myArray.length > 1 && myArray.length < 4){
					var description = this.formModel.description
					var birth = this.formModel.birthDate
					var lastname = myArray[1]
					var firstname = myArray[0]
					if(myArray.length > 2)
						var middlename = myArray[2]
					else {
						var middlename = ''
						//Переменная отвечает введено ли было Отчество в форме
						var middlenameExist = true
					}
					if(
						(firstname.length > 2 && 
						firstname.length < 30 && 
						lastname.length > 2 && 
						lastname.length < 30) &&
						(middlename.length > 2 &&
						middlename.length < 30 || 
						middlenameExist)) 
					{
						if(/^[а-яА-Я_ , ё, Ё]*$/.test(firstname + middlename + lastname)){
							if(birth){
								if((/^[0-9, -]+$/.test( birth)) && 
									 birth.length < 11 && 
									 birth.length > 7 && 
									 (/^[0-9]+$/.test(birth.substr(0, 4))))
								{ 
									birth = new Date(birth)
									if(!(isNaN(birth))) {
										if(description.length < 103) {
											this.formModel.lastName = lastname
											this.formModel.firstName = firstname
											this.formModel.middleName = middlename
											return checkValidate = true
										} else 
											alert('Превышен лимит описания. Пожалуйста уложитесь в 100 символов')
									} else
										alert('Такой даты не существует')
								} else 
									alert('Формат даты введен неправильно\nПример: "2021-12-12"')
							} else 
								alert('Введите дату')
						} else
							alert('Введите ФИО содержащее только русские буквы')
					} else
						alert('Ваше ФИО не соответствует размеру, либо в нем есть лишний пробел')
				} else
					alert('Введите хотя бы Фамилию и Имя')
			} else 
				alert('Введите ФИО')
		},
	}
}

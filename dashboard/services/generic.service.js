import axios from './request.service'

//Класс отвечающий за все запросы
export default class GenericService {
	constructor({ url, name }) {
		this.url = url
		this.name = name
	}
	async fetchAll() {
		try {
			const { data } = await axios.get(`${this.url}/`)
			return data
		} catch(err) {
			throw {
				err,
				error: true,
				message: `${this.name} on All something wrong`,
			}
		}
	}
	async fetchOne(id) {
		try {
			const { data } = await axios.get(`${this.url}/${id}`)
			return data
		} catch(err) {
			throw {
				err,
				error: true,
				message: `${this.name} on FETCHONE something wrong`,
			}
		}
	}
	async update(id, payload) {
		try {
			const { data } = await axios.put(`${this.url}/${id}`, payload)
			alert("Анкета изменена!")
			return data
		} catch(err) {
			throw {
				err,
				error: true,
				message: `${this.name} on UPDATE something wrong`,
			}
		}
	}
	async create(payload) {
		try {
			const { data } = await axios.post(`${this.url}/`, payload)
			alert("Анкета создана!")
			return data
		} catch(err) {
			throw {
				err,
				error: true,
				message: `${this.name} on CREATE something wrong`,
			}
		}
	}
	async delete(id) {
		try {
			await axios.delete(`${this.url}/${id}`)
		} catch(err) {
			throw {
				err,
				error: true,
				message: `${this.name} on DELETLE something wrong`,
			}
		}
	}
}
//Схема полей для формы
export const schema = {
	fields: [	
		{
			type: 'input',
			inputType: 'text',
			label: 'ФИО',
			model: 'fullName',
			placeholder: 'Введите ФИО',
			styleClasess: 'col-md-6',
		},
		{
			type: 'input',
			inputType: 'text',
			label: 'Дата рождения',
			model: 'birthDate',
			placeholder: 'Введите дату рождения',
			styleClasess: 'col-md-6',
		},
		{
			type: 'textArea',
			label: 'Описание',
			model: 'description',
			placeholder: 'Опишите анкету(необязательно)',
			hint: 'макс 100 символов',
			row: 10,
			styleClasess: 'col-md-12',
		},
	]
}

//Модель для формы
export const defaultForm = {
	firstName: '',
	lastName: '',
	middleName: '',
	fullName: '',
	birthDate: '',
	description: ''
}



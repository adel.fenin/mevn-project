//Ключи с именами для реализации таблицы
export const columns = [
	{ key: 'firstName', name: 'Фамилия' },
	{ key: 'lastName', name: 'Имя' },
	{ key: 'middleName', name: 'Отчество' },
	{ key: 'birthDate', name: 'Дата рождения' },
	{ key: 'description', name: 'Описание' },
]

//Данные для реализации кнопок в таблице
export const actions = [
	{
		className: 'btn btn-warning text-white',
		label: 'Редактировать',
		emit: 'onEdit',
		actionKey: '_id',
	},
	{
		className: 'btn btn-danger mt-2',
		label: 'Удалить',
		emit: 'onDelete',
		actionKey: '_id',
		icon: 'icwt-trash'
	},
]


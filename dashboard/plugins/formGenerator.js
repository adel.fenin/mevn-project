import Vue from 'vue'
import FormGenerator from 'vue-form-generator'
import MultiSelect from 'vue-multiselect'

import Paginate from 'vuejs-paginate'

Vue.component('multiselect', MultiSelect)
Vue.component('paginate', Paginate)

import 'vue-multiselect/dist/vue-multiselect.min.css'

Vue.use(FormGenerator)